package com.jst.vm.impl.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jst.vm.api.dto.ItemOperationServiceRequest;
import com.jst.vm.api.dto.ItemOperationServiceResponse;
import com.jst.vm.api.service.ItemOperationService;
import com.jst.vm.impl.main.VendingMachineApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static com.jst.vm.api.dao.entity.Item.ItemType.TOY;
import static com.jst.vm.api.dto.ItemOperationServiceResponse.StatusMessage.E00;
import static com.jst.vm.api.dto.ItemOperationServiceResponse.StatusMessage.E01;
import static com.jst.vm.api.dto.ItemOperationServiceResponse.StatusMessage.E02;
import static com.jst.vm.api.dto.ItemOperationServiceResponse.StatusMessage.E03;
import static com.jst.vm.api.dto.ItemOperationServiceResponse.StatusMessage.NA;
import static com.jst.vm.api.dto.ItemOperationServiceResponse.StatusMessage.OK;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = VendingMachineApplication.class)
@AutoConfigureMockMvc
public class ItemOperationControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ItemOperationService service;

    @Test
    public void getItem_ok() throws Exception {
        given(service.getItem(new ItemOperationServiceRequest("soda"))).willReturn(ItemOperationServiceResponse.getInstance(OK));
        mvc.perform(post("/api/withdraw")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(new ItemOperationServiceRequest("soda"))))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is("OK")));
    }

    @Test
    public void getItem_wrongItemType() throws Exception {
        given(service.getItem(new ItemOperationServiceRequest("houska"))).willReturn(ItemOperationServiceResponse.getInstance(E03));
        mvc.perform(post("/api/withdraw")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(new ItemOperationServiceRequest("houska"))))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is(E03.getMessage())));
    }

    @Test
    public void getItem_noSuchItem() throws Exception {
        given(service.getItem(new ItemOperationServiceRequest("toy"))).willReturn(ItemOperationServiceResponse.getInstance(NA));
        mvc.perform(post("/api/withdraw")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(new ItemOperationServiceRequest("toy"))))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is(NA.getMessage())));
    }

    @Test
    public void getList_ok() throws Exception {
        given(service.getAllItems()).willReturn(
            Arrays.asList(
                ItemOperationServiceResponse.getInstance(1L, "SODA"),
                ItemOperationServiceResponse.getInstance(2L, "CANDY"),
                ItemOperationServiceResponse.getInstance(3L, "TOY")
        ));
        mvc.perform(get("/api/getlist")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].type", is("SODA")))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].type", is("CANDY")))
                .andExpect(jsonPath("$[2].id", is(3)))
                .andExpect(jsonPath("$[2].type", is("TOY")));

    }

    @Test
    public void getList_empty() throws Exception {
        given(service.getAllItems()).willReturn(Arrays.asList());
        mvc.perform(get("/api/getlist")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().string("[]"));
    }

    @Test
    public void addItem_ok() throws Exception {
        given(service.insertItem(new ItemOperationServiceRequest("soda"))).willReturn(ItemOperationServiceResponse.getInstance(1l, OK));
        mvc.perform(post("/api/deposit")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(new ItemOperationServiceRequest("soda"))))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.status", is(OK.getMessage())));
    }

    @Test
    public void addItem_wrongItemType() throws Exception {
        given(service.insertItem(new ItemOperationServiceRequest("pivo"))).willReturn(ItemOperationServiceResponse.getInstance(E03));
        mvc.perform(post("/api/deposit")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(new ItemOperationServiceRequest("pivo"))))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is(E03.getMessage())));
    }

    @Test
    public void addItem_stackFull() throws Exception {
        String message = String.format(E01.getMessage(), TOY);
        given(service.insertItem(new ItemOperationServiceRequest("candy"))).willReturn(ItemOperationServiceResponse.getInstance(message));
        mvc.perform(post("/api/deposit")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(new ItemOperationServiceRequest("candy"))))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is(message)));
    }

    @Test
    public void addItem_UnknownErrorDuringItemInsertion() throws Exception {
        given(service.insertItem(new ItemOperationServiceRequest("toy"))).willReturn(ItemOperationServiceResponse.getInstance(E02));
        mvc.perform(post("/api/deposit")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(new ItemOperationServiceRequest("toy"))))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is(E02.getMessage())));
    }

    @Test
    public void addItem_GeneralUnknownError() throws Exception {
        given(service.insertItem(new ItemOperationServiceRequest("soda"))).willReturn(ItemOperationServiceResponse.getInstance(E00));
        mvc.perform(post("/api/deposit")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(new ItemOperationServiceRequest("soda"))))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is(E00.getMessage())));
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}