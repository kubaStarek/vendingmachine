package com.jst.vm.impl.controller;

import com.jst.vm.api.dto.ItemOperationServiceRequest;
import com.jst.vm.api.dto.ItemOperationServiceResponse;
import com.jst.vm.impl.service.ItemOperationServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static com.jst.vm.api.dao.entity.Item.ItemType.TOY;
import static com.jst.vm.api.dto.ItemOperationServiceResponse.StatusMessage.E00;
import static com.jst.vm.api.dto.ItemOperationServiceResponse.StatusMessage.E01;
import static com.jst.vm.api.dto.ItemOperationServiceResponse.StatusMessage.E02;
import static com.jst.vm.api.dto.ItemOperationServiceResponse.StatusMessage.E03;
import static com.jst.vm.api.dto.ItemOperationServiceResponse.StatusMessage.NA;
import static com.jst.vm.api.dto.ItemOperationServiceResponse.StatusMessage.OK;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ItemOperationControllerTest {

    @Mock
    private ItemOperationServiceImpl service;

    @InjectMocks
    private ItemOperationController controller;

    @Test
    public void getItem_ok() {
        when(service.getItem(new ItemOperationServiceRequest("soda"))).thenReturn(ItemOperationServiceResponse.getInstance(OK));
        ItemOperationServiceResponse res = controller.getItem(new ItemOperationServiceRequest("soda"));
        assertFalse(res.getId().isPresent());
        assertFalse(res.getType().isPresent());
        assertTrue(res.getStatus().isPresent());
        assertEquals(OK.getMessage(), res.getStatus().get());
    }

    @Test
    public void getItem_wrongItemType() {
        when(service.getItem(new ItemOperationServiceRequest("pivo"))).thenReturn(ItemOperationServiceResponse.getInstance(E03));
        ItemOperationServiceResponse res = controller.getItem(new ItemOperationServiceRequest("pivo"));
        assertFalse(res.getId().isPresent());
        assertFalse(res.getType().isPresent());
        assertTrue(res.getStatus().isPresent());
        assertEquals(E03.getMessage(), res.getStatus().get());
    }

    @Test
    public void getItem_noSuchItem() {
        when(service.getItem(new ItemOperationServiceRequest("candy"))).thenReturn(ItemOperationServiceResponse.getInstance(NA));
        ItemOperationServiceResponse res = controller.getItem(new ItemOperationServiceRequest("candy"));
        assertFalse(res.getId().isPresent());
        assertFalse(res.getType().isPresent());
        assertTrue(res.getStatus().isPresent());
        assertEquals(NA.getMessage(), res.getStatus().get());
    }

    @Test
    public void getList_ok() {
        when(service.getAllItems()).thenReturn(Arrays.asList(ItemOperationServiceResponse.getInstance(1L, "SODA")));
        List<ItemOperationServiceResponse> res = controller.getList();
        assertTrue(res.size() == 1);
        assertEquals(Long.valueOf(1), res.get(0).getId().get());
        assertEquals("SODA", res.get(0).getType().get());
    }

    @Test
    public void getList_empty() {
        when(service.getAllItems()).thenReturn(Arrays.asList());
        List<ItemOperationServiceResponse> res = controller.getList();
        assertTrue(res.isEmpty());
    }

    @Test
    public void addItem_ok() {
        when(service.insertItem(new ItemOperationServiceRequest("soda"))).thenReturn(ItemOperationServiceResponse.getInstance(1l, OK));
        ItemOperationServiceResponse res = controller.addItem(new ItemOperationServiceRequest("soda"));
        assertTrue(res.getId().isPresent());
        assertFalse(res.getType().isPresent());
        assertTrue(res.getStatus().isPresent());
        assertEquals(Long.valueOf(1), res.getId().get());
        assertEquals(OK.getMessage(), res.getStatus().get());
    }

    @Test
    public void addItem_wrongItemType() {
        when(service.insertItem(new ItemOperationServiceRequest("houska"))).thenReturn(ItemOperationServiceResponse.getInstance(E03));
        ItemOperationServiceResponse res = controller.addItem(new ItemOperationServiceRequest("houska"));
        assertFalse(res.getId().isPresent());
        assertFalse(res.getType().isPresent());
        assertTrue(res.getStatus().isPresent());
        assertEquals(E03.getMessage(), res.getStatus().get());
    }

    @Test
    public void addItem_stackFull() {
        String message = String.format(E01.getMessage(), TOY);
        when(service.insertItem(new ItemOperationServiceRequest("toy"))).thenReturn(ItemOperationServiceResponse.getInstance(message));
        ItemOperationServiceResponse res = controller.addItem(new ItemOperationServiceRequest("toy"));
        assertFalse(res.getId().isPresent());
        assertFalse(res.getType().isPresent());
        assertTrue(res.getStatus().isPresent());
        assertEquals(message, res.getStatus().get());
    }

    @Test
    public void addItem_UnknownErrorDuringItemInsertion() {
        when(service.insertItem(new ItemOperationServiceRequest("candy"))).thenReturn(ItemOperationServiceResponse.getInstance(E02));
        ItemOperationServiceResponse res = controller.addItem(new ItemOperationServiceRequest("candy"));
        assertFalse(res.getId().isPresent());
        assertFalse(res.getType().isPresent());
        assertTrue(res.getStatus().isPresent());
        assertEquals(E02.getMessage(), res.getStatus().get());
    }

    @Test
    public void addItem_GeneralUnknownError() {
        when(service.insertItem(new ItemOperationServiceRequest("soda"))).thenReturn(ItemOperationServiceResponse.getInstance(E00));
        ItemOperationServiceResponse res = controller.addItem(new ItemOperationServiceRequest("soda"));
        assertFalse(res.getId().isPresent());
        assertFalse(res.getType().isPresent());
        assertTrue(res.getStatus().isPresent());
        assertEquals(E00.getMessage(), res.getStatus().get());
    }

}