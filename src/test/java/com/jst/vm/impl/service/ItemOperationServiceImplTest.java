package com.jst.vm.impl.service;

import com.jst.vm.api.dao.entity.Item;
import com.jst.vm.api.dto.ItemOperationServiceRequest;
import com.jst.vm.api.dto.ItemOperationServiceResponse;
import com.jst.vm.api.exception.ItemDaoException;
import com.jst.vm.impl.dao.ItemDAOInMemoryImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.jst.vm.api.dao.entity.Item.ItemType.CANDY;
import static com.jst.vm.api.dao.entity.Item.ItemType.SODA;
import static com.jst.vm.api.dao.entity.Item.ItemType.TOY;
import static com.jst.vm.api.dto.ItemOperationServiceResponse.StatusMessage.E00;
import static com.jst.vm.api.dto.ItemOperationServiceResponse.StatusMessage.E01;
import static com.jst.vm.api.dto.ItemOperationServiceResponse.StatusMessage.E02;
import static com.jst.vm.api.dto.ItemOperationServiceResponse.StatusMessage.E03;
import static com.jst.vm.api.dto.ItemOperationServiceResponse.StatusMessage.NA;
import static com.jst.vm.api.dto.ItemOperationServiceResponse.StatusMessage.OK;
import static com.jst.vm.api.exception.ItemDaoException.ErrorStatus.GENERAL_ERROR;
import static com.jst.vm.api.exception.ItemDaoException.ErrorStatus.STACK_FULL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ItemOperationServiceImplTest {

    @Mock
    private ItemDAOInMemoryImpl dao;

    @InjectMocks
    private ItemOperationServiceImpl service;

    @Test
    public void getItem_ok() {
        when(dao.getByType(TOY)).thenReturn(Optional.of(new Item(1L, TOY)));
        ItemOperationServiceResponse res = service.getItem(new ItemOperationServiceRequest("ToY"));
        assertFalse(res.getId().isPresent());
        assertFalse(res.getType().isPresent());
        assertTrue(res.getStatus().isPresent());
        assertEquals(OK.getMessage(), res.getStatus().get());
    }

    @Test
    public void getItem_wrongItemType() {
        ItemOperationServiceResponse res = service.getItem(new ItemOperationServiceRequest("houska"));
        assertFalse(res.getId().isPresent());
        assertFalse(res.getType().isPresent());
        assertTrue(res.getStatus().isPresent());
        assertEquals(E03.getMessage(), res.getStatus().get());
    }

    @Test
    public void getItem_noSuchItem() {
        when(dao.getByType(CANDY)).thenReturn(Optional.empty());
        ItemOperationServiceResponse res = service.getItem(new ItemOperationServiceRequest("candy"));
        assertFalse(res.getId().isPresent());
        assertFalse(res.getType().isPresent());
        assertTrue(res.getStatus().isPresent());
        assertEquals(NA.getMessage(), res.getStatus().get());
    }

    @Test
    public void getAllItems_ok() {
        when(dao.getAll()).thenReturn(
            Arrays.asList(
                new Item(1l, SODA),
                new Item(2l, CANDY),
                new Item(3l, TOY)
        ));

        List<ItemOperationServiceResponse> res = service.getAllItems();
        assertTrue(res.size() == 3);
        assertEquals(Long.valueOf(1), res.get(0).getId().get());
        assertEquals("SODA", res.get(0).getType().get());
        assertEquals(Long.valueOf(2), res.get(1).getId().get());
        assertEquals("CANDY", res.get(1).getType().get());
        assertEquals(Long.valueOf(3), res.get(2).getId().get());
        assertEquals("TOY", res.get(2).getType().get());
    }

    @Test
    public void getAllItems_empty() {
        when(dao.getAll()).thenReturn(Arrays.asList());
        List<ItemOperationServiceResponse> res = service.getAllItems();
        assertTrue(res.isEmpty());
    }

    @Test
    public void insertItem_ok() throws ItemDaoException {
        when(dao.insert(SODA)).thenReturn(new Item(1l, SODA));
        ItemOperationServiceResponse res = service.insertItem(new ItemOperationServiceRequest("soda"));
        assertTrue(res.getId().isPresent());
        assertFalse(res.getType().isPresent());
        assertTrue(res.getStatus().isPresent());
        assertEquals(Long.valueOf(1), res.getId().get());
        assertEquals(OK.getMessage(), res.getStatus().get());
    }

    @Test
    public void insertItem_wrongItemType() {
        ItemOperationServiceResponse res = service.insertItem(new ItemOperationServiceRequest("pivo"));
        assertFalse(res.getId().isPresent());
        assertFalse(res.getType().isPresent());
        assertTrue(res.getStatus().isPresent());
        assertEquals(E03.getMessage(), res.getStatus().get());
    }

    @Test
    public void insertItem_stackFull() throws ItemDaoException {
        when(dao.insert(SODA)).thenThrow(new ItemDaoException("Dao Error", STACK_FULL));
        ItemOperationServiceResponse res = service.insertItem(new ItemOperationServiceRequest("SODA"));
        assertFalse(res.getId().isPresent());
        assertFalse(res.getType().isPresent());
        assertTrue(res.getStatus().isPresent());
        assertEquals(String.format(E01.getMessage(),SODA), res.getStatus().get());
    }

    @Test
    public void insertItem_UnknownErrorDuringItemInsertion() throws ItemDaoException {
        when(dao.insert(TOY)).thenThrow(new ItemDaoException("Dao Error", GENERAL_ERROR));
        ItemOperationServiceResponse res = service.insertItem(new ItemOperationServiceRequest("toy"));
        assertFalse(res.getId().isPresent());
        assertFalse(res.getType().isPresent());
        assertTrue(res.getStatus().isPresent());
        assertEquals(E02.getMessage(), res.getStatus().get());
    }

    @Test
    public void insertItem_GeneralUnknownError() throws ItemDaoException {
        when(dao.insert(CANDY)).thenThrow(new RuntimeException());
        ItemOperationServiceResponse res = service.insertItem(new ItemOperationServiceRequest("CanDy"));
        assertFalse(res.getId().isPresent());
        assertFalse(res.getType().isPresent());
        assertTrue(res.getStatus().isPresent());
        assertEquals(E00.getMessage(), res.getStatus().get());
    }
}