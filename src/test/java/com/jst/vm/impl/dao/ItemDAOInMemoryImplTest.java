package com.jst.vm.impl.dao;

import com.jst.vm.api.dao.entity.Item;
import com.jst.vm.api.exception.ItemDaoException;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.jst.vm.api.dao.entity.Item.ItemType;
import static com.jst.vm.api.dao.entity.Item.ItemType.CANDY;
import static com.jst.vm.api.dao.entity.Item.ItemType.SODA;
import static com.jst.vm.api.dao.entity.Item.ItemType.TOY;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ItemDAOInMemoryImplTest {

    private ItemDAOInMemoryImpl dao;

    @Before
    public void init(){
        dao = new ItemDAOInMemoryImpl();
        dao.init();
    }

    @Test
    public void testInsertItem_ok() throws ItemDaoException {
        Item item = dao.insert(SODA);
        assertEquals(SODA, item.getType());
        assertEquals(Long.valueOf(1), Long.valueOf(item.getId()));
    }

    @Test(expected = ItemDaoException.class)
    public void testInsertItem_toManyItems() throws ItemDaoException {
        for (int i = 0; i < 100 ; i++) {
            dao.insert(SODA);
        }
    }

    @Test
    public void testGetItem_ok() throws ItemDaoException {
        dao.insert(CANDY);
        Optional<Item> item = dao.getByType(CANDY);
        assertTrue(item.isPresent());
        assertEquals(CANDY, item.get().getType());
        assertEquals(Long.valueOf(1), Long.valueOf(item.get().getId()));
    }

    @Test
    public void testGetItem_correctOrder() throws ItemDaoException {
        dao.insert(TOY);
        dao.insert(TOY);
        Optional<Item> item = dao.getByType(TOY);
        assertTrue(item.isPresent());
        assertEquals(TOY, item.get().getType());
        assertEquals(Long.valueOf(2), Long.valueOf(item.get().getId()));
    }

    @Test
    public void testGetItem_empty(){
        Optional<Item> soda = dao.getByType(SODA);
        assertTrue(!soda.isPresent());
    }

    @Test
    public void testGetAllItems_ok(){
        List<ItemType> items = Arrays.asList(SODA, SODA, CANDY, TOY, CANDY, CANDY, SODA, TOY);
        items.forEach(i-> {
            try {
                dao.insert(i);
            } catch (ItemDaoException e) {
                e.printStackTrace();
            }
        });

        List<Item> list = dao.getAll();
        for (int i = 0; i < list.size() ; i++) {
            assertEquals(items.get(i), list.get(i).getType());
        }
    }

    @Test
    public void testGetAllItems_empty(){
        List<Item> list = dao.getAll();
        assertTrue(list.isEmpty());
    }
}