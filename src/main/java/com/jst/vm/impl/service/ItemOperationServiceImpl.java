package com.jst.vm.impl.service;

import com.jst.vm.api.dao.ItemDAO;
import com.jst.vm.api.dao.entity.Item;
import com.jst.vm.api.dto.ItemOperationServiceRequest;
import com.jst.vm.api.dto.ItemOperationServiceResponse;
import com.jst.vm.api.exception.ItemDaoException;
import com.jst.vm.api.service.ItemOperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.jst.vm.api.dao.entity.Item.ItemType;
import static com.jst.vm.api.dto.ItemOperationServiceResponse.StatusMessage.E00;
import static com.jst.vm.api.dto.ItemOperationServiceResponse.StatusMessage.E01;
import static com.jst.vm.api.dto.ItemOperationServiceResponse.StatusMessage.E02;
import static com.jst.vm.api.dto.ItemOperationServiceResponse.StatusMessage.E03;
import static com.jst.vm.api.dto.ItemOperationServiceResponse.StatusMessage.NA;
import static com.jst.vm.api.dto.ItemOperationServiceResponse.StatusMessage.OK;
import static com.jst.vm.api.exception.ItemDaoException.ErrorStatus.STACK_FULL;

@Service
public class ItemOperationServiceImpl implements ItemOperationService {

    @Autowired
    private ItemDAO dao;

    @Override
    public ItemOperationServiceResponse getItem(ItemOperationServiceRequest request)  {
        Optional<ItemType> type = ItemType.valueOfOrEmpty(request.getType());
        if(!type.isPresent()) return ItemOperationServiceResponse.getInstance(E03);

        Optional<Item> daoResponse = dao.getByType(type.get());
        return ItemOperationServiceResponse.getInstance(daoResponse.isPresent() ? OK : NA);
    }

    @Override
    public List<ItemOperationServiceResponse> getAllItems() {
        List<Item> daoResponse = dao.getAll();
        return daoResponse.stream()
            .map( item -> ItemOperationServiceResponse.getInstance(item.getId(), item.getType().name()))
            .collect(Collectors.toList());
    }

    @Override
    public ItemOperationServiceResponse insertItem(ItemOperationServiceRequest request) {
        Optional<ItemType> type = ItemType.valueOfOrEmpty(request.getType());
        if(!type.isPresent()) return ItemOperationServiceResponse.getInstance(E03);

        try {
            Item daoResponse = dao.insert(type.get());
            return ItemOperationServiceResponse.getInstance(daoResponse.getId(), OK);
        } catch (ItemDaoException e) {
            if(e.getErrorStatus() == STACK_FULL)
                return ItemOperationServiceResponse.getInstance(String.format(E01.getMessage(), type.get()));
            else
                return ItemOperationServiceResponse.getInstance(E02);
        } catch (Exception e){
            return ItemOperationServiceResponse.getInstance(E00);
        }
    }
}
