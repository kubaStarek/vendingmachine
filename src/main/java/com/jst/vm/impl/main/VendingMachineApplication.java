package com.jst.vm.impl.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.jst.vm"})
public class VendingMachineApplication {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(VendingMachineApplication.class);
        app.run(args);
    }
}
