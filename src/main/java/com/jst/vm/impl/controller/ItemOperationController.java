package com.jst.vm.impl.controller;

import com.jst.vm.api.dto.ItemOperationServiceRequest;
import com.jst.vm.api.dto.ItemOperationServiceResponse;
import com.jst.vm.api.service.ItemOperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping(path = "/api")
public class ItemOperationController {

    @Autowired
    private ItemOperationService service;

    @RequestMapping(method = RequestMethod.POST, value="/deposit")
    @ResponseBody
    public ItemOperationServiceResponse addItem(@RequestBody ItemOperationServiceRequest request)  {
        return service.insertItem(request);
    }

    @RequestMapping(method = RequestMethod.POST, value="/withdraw")
    @ResponseBody
    public ItemOperationServiceResponse getItem(@RequestBody ItemOperationServiceRequest request) {
        return service.getItem(request);
    }

    @RequestMapping(method = RequestMethod.GET, value="/getlist")
    @ResponseBody
    public List<ItemOperationServiceResponse> getList() {
        return service.getAllItems();
    }

}

