package com.jst.vm.impl.dao;

import com.jst.vm.api.dao.ItemDAO;
import com.jst.vm.api.dao.entity.Item;
import com.jst.vm.api.exception.ItemDaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import static com.jst.vm.api.dao.entity.Item.ItemType;
import static com.jst.vm.api.exception.ItemDaoException.ErrorStatus.STACK_FULL;

@Component
public class ItemDAOInMemoryImpl implements ItemDAO {

    private int maxStackSize;
    private AtomicLong counter;
    private Map<ItemType, Deque<Item>> itemStore;

    @Autowired
    private Environment env;

    @PostConstruct
    protected void init(){
        maxStackSize = (env == null ? 10 : Integer.parseInt(env.getProperty("machine.max_item_capacity")));
        counter = new AtomicLong();
        itemStore = new HashMap<>();
        for (ItemType itemType : ItemType.values()){
            itemStore.put(itemType, new LinkedList<>());
        }
    }

    @Override
    public synchronized Optional<Item> getByType(ItemType itemType) {
        return Optional.ofNullable(itemStore.get(itemType).pollLast());
    }

    @Override
    public synchronized List<Item> getAll() {
        return itemStore
            .keySet()
            .stream()
                .flatMap(key -> itemStore.get(key).stream())
                .sorted(Comparator.comparing(Item::getId))
                .collect(Collectors.toList());
    }

    @Override
    public synchronized Item insert(ItemType itemType) throws ItemDaoException {
        Deque<Item> currentStack = itemStore.get(itemType);
        if(currentStack.size() >= maxStackSize)
            throw new ItemDaoException("Stack for item type "+itemType.name()+" exhausted.", STACK_FULL);

        currentStack.offer(new Item(counter.incrementAndGet(), itemType));
        return currentStack.peekLast();
    }
}
