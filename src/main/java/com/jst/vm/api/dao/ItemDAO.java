package com.jst.vm.api.dao;

import com.jst.vm.api.dao.entity.Item;
import com.jst.vm.api.exception.ItemDaoException;

import java.util.List;
import java.util.Optional;

import static com.jst.vm.api.dao.entity.Item.ItemType;

public interface ItemDAO {

    Optional<Item> getByType(ItemType itemType);

    List<Item> getAll();

    Item insert(ItemType itemType) throws ItemDaoException;

}
