package com.jst.vm.api.dao.entity;

import java.util.Objects;
import java.util.Optional;

public class Item {

    private final Long id;
    private final ItemType type;

    public Item(Long id, ItemType itemType) {
        this.id = id;
        this.type = itemType;
    }

    public Long getId() {
        return id;
    }

    public ItemType getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Item)) return false;
        Item item = (Item) o;
        return Objects.equals(getId(), item.getId()) &&
                getType() == item.getType();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getType());
    }

    public enum ItemType {
        SODA, CANDY, TOY;

        public static Optional<ItemType> valueOfOrEmpty(String itemType) {
            try {
                return Optional.of(ItemType.valueOf(itemType.toUpperCase()));
            } catch (IllegalArgumentException e){
                return Optional.empty();
            }
        }
    }
}
