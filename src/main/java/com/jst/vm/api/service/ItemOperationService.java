package com.jst.vm.api.service;

import com.jst.vm.api.dto.ItemOperationServiceRequest;
import com.jst.vm.api.dto.ItemOperationServiceResponse;

import java.util.List;

public interface ItemOperationService {

    ItemOperationServiceResponse getItem(ItemOperationServiceRequest request);

    List<ItemOperationServiceResponse> getAllItems();

    ItemOperationServiceResponse insertItem(ItemOperationServiceRequest request);

}
