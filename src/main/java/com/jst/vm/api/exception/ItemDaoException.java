package com.jst.vm.api.exception;

public class ItemDaoException extends Exception {

    private ErrorStatus errorStatus;

    public ItemDaoException(String message, ErrorStatus errorStatus) {
        super(message);
        this.errorStatus = errorStatus;
    }

    public ItemDaoException(String message, Throwable cause, ErrorStatus errorStatus) {
        super(message, cause);
        this.errorStatus = errorStatus;
    }

    public ItemDaoException(Throwable cause, ErrorStatus errorStatus) {
        super(cause);
        this.errorStatus = errorStatus;
    }

    public ErrorStatus getErrorStatus() {
        return errorStatus;
    }

    public enum ErrorStatus{
        STACK_FULL, GENERAL_ERROR;
    }
}
