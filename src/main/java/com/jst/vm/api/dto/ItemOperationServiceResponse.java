package com.jst.vm.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Objects;
import java.util.Optional;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ItemOperationServiceResponse {

    private Optional<Long> id;
    private Optional<String> type;
    private Optional<String> status;

    private ItemOperationServiceResponse(Optional<Long> id, Optional<String> type, Optional<String> status) {
        this.id = id;
        this.type = type;
        this.status = status;
    }

    public static ItemOperationServiceResponse getInstance(StatusMessage statusMessage) {
        return new ItemOperationServiceResponse(Optional.empty(), Optional.empty(), Optional.ofNullable(statusMessage.getMessage()));
    }

    public static ItemOperationServiceResponse getInstance(String statusMessage) {
        return new ItemOperationServiceResponse(
            Optional.empty(), Optional.empty(), Optional.ofNullable(statusMessage));
    }

    public static ItemOperationServiceResponse getInstance(Long id, StatusMessage statusMessage) {
        return new ItemOperationServiceResponse(Optional.ofNullable(id), Optional.empty(), Optional.ofNullable(statusMessage.getMessage()));
    }

    public static ItemOperationServiceResponse getInstance(Long id, String type) {
        return new ItemOperationServiceResponse(Optional.ofNullable(id), Optional.ofNullable(type), Optional.empty());
    }

    public Optional<Long> getId() {
        return id;
    }

    public void setId(Optional<Long> id) {
        this.id = id;
    }

    public Optional<String> getType() {
        return type;
    }

    public void setType(Optional<String> type) {
        this.type = type;
    }

    public Optional<String> getStatus() {
        return status;
    }

    public void setStatus(Optional<String> status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ItemOperationServiceResponse)) return false;
        ItemOperationServiceResponse that = (ItemOperationServiceResponse) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getType(), that.getType()) &&
                Objects.equals(getStatus(), that.getStatus());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getType(), getStatus());
    }

    public enum StatusMessage {
        OK("OK"),
        NA("N/A"),
        E00("E00 - Unknown error"),
        E01("E01 - Stack for item type %s is full"),
        E02("E02 - Unknown error during item insertion"),
        E03("E03 - Wrong input item type");

        private String message;

        StatusMessage(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }
}
