package com.jst.vm.api.dto;

import java.util.Objects;

public class ItemOperationServiceRequest {

    private String type;

    public ItemOperationServiceRequest() {}

    public ItemOperationServiceRequest(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ItemOperationServiceRequest)) return false;
        ItemOperationServiceRequest that = (ItemOperationServiceRequest) o;
        return Objects.equals(getType(), that.getType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getType());
    }
}
