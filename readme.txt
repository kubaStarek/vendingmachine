Vending Machine (by Jakub Starek 2019)
...................................................
HOMEWORK FOR BACKEND DEVELOPER POSITION IN BROADCOM
---------------------------------------------------

o  Prerequisites
    - JDK 8
    - Maven 3
    - Git

o How to build and run the app
    - clone vendingmachine to your local machine
    - go to vendingmachine directory and run
        * install_and_run.bat for Windows
        * install_and_run.sh for Linux

o How to use the app
    - by default you can access REST API of the app on http://localhost:8083
    - if the port 8083 is occupied on your machine, you can change it in application.properties file; field server.port
    - you can also change maximal item capacity by changing value of machine.max_item_capacity in application.properties; default value is 10